<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport"><!-- Bootstrap CSS -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">
  <title>農業風水師</title>
  <style>
     #up{
     background-color: #2A6041!important;
     }
  </style>
  <style>
     #div {
     margin-bottom: 10px;
     display: flex;
     align-items: center;
     }

     #label {
     display: inline-block;
     width: 300px;
     }

     #input:invalid+span:after {
     content: '✖';
     padding-left: 5px;
     }

     #input:valid+span:after {
     content: '✓';
     padding-left: 5px;
     }

  </style>
</head>
<body class="text-center">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="up">
            <a class="navbar-brand" href="first.html"><span class="h3 mx-1"><木>生產單</span></a> <button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                  <a class="nav-link" href="main.php">羅盤</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="reservation.php">預約生產單</a>
                </li>
              </ul>
          </div>
      </nav><br>
  </div>
  <div class="container">
    <h2>建立生產單</h2>
    <hr>
    <?php
            include_once 'phpscripts/Manager.php';
            
            function base_show() {
              $address = new AddressManager();
              echo '
                <form action="createProductList.php" method="get">
                  <div>
                    <label for="date">種植日期:</label> <input id="date" name="date" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" required="" type="date" > <span class="validity"></span>
                  </div>
                  <p id=except_date></p>

                  <!--
                  <div>
                    <label for="cropName">作物名稱:</label> <input id="cropName" name="cropName" required="required" type="text"> <span class="validity"></span>
                  </div>
                  -->
                  <div>
                    <label for="cropName">作物名稱:</label> 
                      <select name="cropName"> <span class="validity"></span>
                        <option value="洋香瓜">洋香瓜</option>
                        <option value="小番茄">小番茄</option>
                      </select>
                  </div>
                  <input id="villageNo" name="villageNo" required="required" type="hidden" value="'.$_SESSION['villageNo'].'"> <span class="validity"></span>
                  <p>栽種地點：'.$address->getAddressName($_SESSION['villageNo']).'</p>

                  <hr>
                  <input class="btn btn-outline-success" onclick="history.back()" type="button" value="回到上一頁"> <input class="btn btn-outline-success" name="submitted" type="submit" value="建立生產單"> 
                </form>';

          }

            function run($_date, $_cropName, $_villageNo) {
              $listController = new ListController();
              $year           = date('Y', strtotime($_date));
              $month          = date('m', strtotime($_date));
              $day            = date('d', strtotime($_date));
              $cropName       = $_cropName;
              //echo $year . $month . $day . $cropName . $_villageNo;
              $listController->createNewProductList($day, $month, $year, $cropName, $_villageNo);

              header("Location:main.php");
            }

              if (isset($_GET['date']) && isset($_GET['cropName']) && isset($_GET['villageNo'])) {
                run($_GET['date'], $_GET['cropName'], $_GET['villageNo']);
              } else {
              	//print_r($_GET);
                base_show();
              }
            ?>
  </div><!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js">
  </script> 
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js">
  </script> 
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js">
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
  </script>
  <script>
      $(document).on('change', '#date', function(){
       var date = $('#date').val();//注意:selected前面有個空格！
        $.ajax({
            url:"phpscripts/Manager.php",       
            method:"POST",
            data:{
              reservation_date: date
            },          
            success:function(res){  
               if (new Date(date) < new Date()) {
                //alert('注意!你選擇了過去的時間點');
               }
               $('#except_date').html("預計收穫時間："+res);
            }

        });//end ajax
    });
  </script>
</body>
</html>
