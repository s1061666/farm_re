<?php session_start(); ?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
  integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
  integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
  integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://www.jqueryflottutorial.com/js/lib/jquery-1.8.3.min.js" type='text/javascript'></script>
<script type="text/javascript" src="http://www.jqueryflottutorial.com/js/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="http://www.jqueryflottutorial.com/js/flot/jquery.flot.time.js"></script>
<script type="text/javascript" src="http://www.jqueryflottutorial.com/js/flot/jshashtable-2.1.js"></script>
<script type="text/javascript" src="http://www.jqueryflottutorial.com/js/flot/jquery.numberformatter-1.2.3.min.js">
</script>
<script type="text/javascript" src="http://www.jqueryflottutorial.com/js/flot/jquery.flot.symbol.js"></script>
<script type="text/javascript" src="http://www.jqueryflottutorial.com/js/flot/jquery.flot.axislabels.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<?php
// $dsn        = "mysql:host=localhost;dbname=hacktonfarm;charset=utf8";
// $username   = "dluser";
// $password   = "dluser@515";
// $ar         = array();
// $ar3        = array();
// $ar4        = array();
// $ar1        = array();
// $ar2        = array();
// $cropid     = "";
// $connection = new PDO($dsn, $username, $password);
include_once "./connect_local_test.php";
$farmtest   = $con->query("SELECT start_date,farm_name,cropid,id FROM test_plant WHERE farmer_id='" . $_SESSION["log"]["id"] . "'");
foreach ($farmtest as $farmt) {
    array_push($ar3, $farmt['farm_name']);
    array_push($ar4, $farmt['start_date']);
    array_push($ar, $farmt['cropid']);
    array_push($ar1, $farmt['id']);
}
if (isset(($_POST['chooseday']))) {
    //$plantid=$_POST['plantid'];
    $chooseday = $_POST['chooseday'];
    $startday  = "";
    $farmt     = $con->query("SELECT start_date,farm_name,cropid,id,cropname FROM test_plant WHERE id='" . $_SESSION["earthid"] . "'");
    foreach ($farmt as $farm) {
        $plantdate = $farm['start_date'];
        $cropid    = $farm['cropid'];
        $cropname  = $farm['cropname'];
    }
    if ($chooseday == 1) {
        $startday = date("Y/m/d", strtotime($plantdate . "-1 Months"));
    } elseif ($chooseday == 2) {
        $startday = date("Y/m/d", strtotime($plantdate . "-1 Years"));

    }
    $moneydate  = $con->query("SELECT price,moneydate,produce FROM moneydate WHERE moneydate BETWEEN CAST('" . $startday . "'AS DATE)AND CAST('" . $plantdate . "'AS DATE) AND cropid ='" . $cropid . "'");
    $moneydate1 = $con->query("SELECT price,moneydate,produce FROM moneydate WHERE moneydate BETWEEN CAST('" . $startday . "'AS DATE)AND CAST('" . $plantdate . "'AS DATE) AND cropid ='" . $cropid . "'");
    $moneydate2 = $con->query("SELECT price,moneydate,produce FROM moneydate WHERE moneydate BETWEEN CAST('" . $startday . "'AS DATE)AND CAST('" . $plantdate . "'AS DATE) AND cropid ='" . $cropid . "'");
}

?>
<script>
function get_date() {
  var bookdate = document.getElementById('bookdate').value;
  document.location.href = "?bookdate=" + bookdate;
}
var data1 = [ <
  ?
  php
  foreach($moneydate as $moneyd) {
    print("[gd(".str_replace("/", ",", $moneyd["moneydate"]).
      "),".$moneyd["price"].
      "],");
  } ?
  >
];

var data2 = [ <
  ?
  php
  foreach($moneydate2 as $moneyd2) {
    print("[gd(".str_replace("/", ",", $moneyd2["moneydate"]).
      "),".$moneyd2["produce"].
      "],");
  } ?
  >
];
var dataset = [{
    label: "平均交易價(元/公斤)",
    data: data1,
    points: {
      symbol: "triangle"
    }
  },
  {
    label: "平均交易量",
    data: data2,
    yaxis: 2
  }
];

var options = {
  series: {
    lines: {
      show: true
    },
    points: {
      radius: 3,
      fill: true,
      show: true
    }
  },
  xaxis: {
    mode: "time",
    tickSize: [1, "day"],
    tickLength: 0,
    axisLabel: new Date(),
    axisLabelUseCanvas: true,
    axisLabelFontSizePixels: 12,
    axisLabelFontFamily: 'Verdana, Arial',
    axisLabelPadding: 10
  },
  yaxes: [{
    axisLabel: "平均交易價(元/公斤)",
    axisLabelUseCanvas: true,
    axisLabelFontSizePixels: 12,
    axisLabelFontFamily: 'Verdana, Arial',
    axisLabelPadding: 3,
    tickFormatter: function(v, axis) {
      return $.formatNumber(v, {
        format: "#,###",
        locale: "us"
      });
    }
  }, {
    position: "right",
    axisLabel: "平均交易量",
    axisLabelUseCanvas: true,
    axisLabelFontSizePixels: 12,
    axisLabelFontFamily: 'Verdana, Arial',
    axisLabelPadding: 3
  }],
  legend: {
    noColumns: 0,
    labelBoxBorderColor: "#000000",
    position: "nw"
  },
  grid: {
    hoverable: true,
    borderWidth: 2,
    borderColor: "#633200",
    backgroundColor: {
      colors: ["#ffffff", "#EDF5FF"]
    }
  },
  colors: ["#FF0000", "#0022FF"]
};

$(document).ready(function() {
  $.plot($("#flot-placeholder1"), dataset, options);
  $("#flot-placeholder1").UseTooltip();
});




function gd(year, month, day) {
  return new Date(year - 1911, month - 1, day).getTime();
}

var previousPoint = null,
  previousLabel = null;
var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

$.fn.UseTooltip = function() {
  $(this).bind("plothover", function(event, pos, item) {
    if (item) {
      if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
        previousPoint = item.dataIndex;
        previousLabel = item.series.label;
        $("#tooltip").remove();

        var x = item.datapoint[0];
        var y = item.datapoint[1];

        var color = item.series.color;
        var month = new Date(x).getMonth();
        var date = (new Date(x).getFullYear() + 1911) + "-" + (new Date(x).getMonth() + 1) + "-" + new Date(x)
          .getDate();
        //console.log(item);

        if (item.seriesIndex == 0) {
          showTooltip(item.pageX,
            item.pageY,
            color,
            "<strong>" + item.series.label + "</strong><br>" + date + " : <strong>" + y + "</strong>(USD)");
        } else {
          showTooltip(item.pageX,
            item.pageY,
            color,
            "<strong>" + item.series.label + "</strong><br>" + date + " : <strong>" + y + "</strong>(%)");
        }
      }
    } else {
      $("#tooltip").remove();
      previousPoint = null;
    }
  });
};

function showTooltip(x, y, color, contents) {
  $('<div id="tooltip">' + contents + '</div>').css({
    position: 'absolute',
    display: 'none',
    top: y - 40,
    left: x - 120,
    border: '2px solid ' + color,
    padding: '3px',
    'font-size': '9px',
    'border-radius': '5px',
    'background-color': '#fff',
    'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
    opacity: 0.9
  }).appendTo("body").fadeIn(200);
}
</script>
<style>
.navbar-light .navbar-brand {
  color: #ffffff;
}

.navbar-light .navbar-nav .nav-link {
  color: rgb(255, 255, 255);
}

#up {
  background-color: #2A6041 !important;
}

.carousel {
  perspective: 500px;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-image: url(image/bg1.jpg) !important;
  background-repeat: no-repeat;
  background-position: center 20%;
}

.slider {
  width: 50%;
  margin: 100px auto;
}

.slick-slide img {
  margin: auto;
}

/* Arrows */
.slick-prev,
.slick-next {
  font-size: 0;
  line-height: 0;
  position: absolute;
  border: none;
  background: transparent;
}

.slick-prev:hover,
.slick-prev:focus,
.slick-next:hover,
.slick-next:focus {
  color: transparent;
  outline: none;
  background: transparent;
}

.slick-prev:hover:before,
.slick-prev:focus:before,
.slick-next:hover:before,
.slick-next:focus:before {
  opacity: 1;
}

.slick-prev.slick-disabled:before,
.slick-next.slick-disabled:before {
  opacity: .25;
}

.slick-prev:before,
.slick-next:before {
  font-family: 'slick';
  font-size: 20px;
  line-height: 1;
  opacity: .75;
  color: white;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

.slick-prev {
  left: -25px;
}

[dir='rtl'] .slick-prev {
  right: -25px;
  left: auto;
}

.slick-prev:before {
  content: '←';
}

[dir='rtl'] .slick-prev:before {
  content: '→';
}

.slick-next {
  right: -25px;
}

[dir='rtl'] .slick-next {
  right: auto;
  left: -25px;
}

.slick-next:before {
  content: '→';
}

[dir='rtl'] .slick-next:before {
  content: '←';
}
</style>
<style>
.navbar-light .navbar-brand {
  color: #ffffff;
}

.navbar-light .navbar-nav .nav-link {
  color: rgb(255, 255, 255);
}

#up {
  background-color: #2A6041 !important;
}

.carousel {
  perspective: 500px;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-image: url(image/bg1.jpg) !important;
  background-repeat: no-repeat;
  background-position: center 20%;
}

.slider {
  width: 50%;
  margin: 100px auto;
}

.slick-slide img {
  margin: auto;
}

/* Arrows */
.slick-prev,
.slick-next {
  font-size: 0;
  line-height: 0;
  position: absolute;
  border: none;
  background: transparent;
}

.slick-prev:hover,
.slick-prev:focus,
.slick-next:hover,
.slick-next:focus {
  color: transparent;
  outline: none;
  background: transparent;
}

.slick-prev:hover:before,
.slick-prev:focus:before,
.slick-next:hover:before,
.slick-next:focus:before {
  opacity: 1;
}

.slick-prev.slick-disabled:before,
.slick-next.slick-disabled:before {
  opacity: .25;
}

.slick-prev:before,
.slick-next:before {
  font-family: 'slick';
  font-size: 20px;
  line-height: 1;
  opacity: .75;
  color: white;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

.slick-prev {
  left: -25px;
}

[dir='rtl'] .slick-prev {
  right: -25px;
  left: auto;
}

.slick-prev:before {
  content: '←';
}

[dir='rtl'] .slick-prev:before {
  content: '→';
}

.slick-next {
  right: -25px;
}

[dir='rtl'] .slick-next {
  right: auto;
  left: -25px;
}

.slick-next:before {
  content: '→';
}

[dir='rtl'] .slick-next:before {
  content: '←';
}
</style>
</head>

<body class="text-center">
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="up">
      <a class="navbar-brand" href="first.html"> <span class="h3 mx-1">農業風水師</span></a> <button
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"
        class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button"><span
          class="navbar-toggler-icon"></span></button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="main_2.html">首頁<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="woodtest.php">預約生產單</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="earthcontroller.php">木</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="moneytest1.php">金</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="login.php">登入</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="register.php">註冊</a>
          </li>
        </ul>

      </div>
    </nav><br>
    <div class="jumbotron jumbotron-fluid" style="background-color = rgb('#FFFFBB')" ;>
      <div class="container">
        <h1 class="display-4">金</h1>
        <p class="lead">市場觀測塔</p>

      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <form method="post">
            <select name="chooseday" id="chooseday" class="form-control">
              <option value="1">預收期一個月趨勢</option>
              <option value="2">預收期一年趨勢</option>
            </select>
            <!--<select name="plantid" class="form-control">
		/*<?php
for ($i = 0; $i < count($ar3); $i++) {
    print("<option value='" . $ar1[$i] . "'>" . $ar3[$i] . "</option>");
}
?>*/
	</select>-->
            <input type='submit' class='btn-success' value='查看'>
          </form>
          <div style="width:450px;height:300px;text-align:center;margin:10px">
            <?php if ($_POST["chooseday"] == 1) {echo ("<h3>預收期一月趨勢</h3>");} elseif ($_POST["chooseday"] == 2) {echo ("<h3>預收期一年趨勢</h3>");} ?>
            <h3>水果產品交易價量走勢圖</h3>
            <div id="flot-placeholder1" style="width:1000px;height:500px;margin:0 auto"></div>
            <br>
            <table class="table">
              <tr>
                <td>品種</td>
                <td>日期</td>
                <td>交易量</td>
                <td>交易價</td>
              </tr>
              <?php
foreach ($moneydate1 as $moneyd1) {
    print("<tr><td>" . $cropname . "</td><td>" . $moneyd1['moneydate'] . "</td><td>" . $moneyd1['produce'] . "</td><td>" . $moneyd1['price'] . "</td></tr>");
}
?>
            </table>
            <!--<div id="flot-placeholder1" style="width:1000px;height:500px;margin:0 auto"></div>--!>
</div>
</div>
</div>
</body>
</html>