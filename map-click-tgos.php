<!--程式碼範例-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
<title>土地定位管理</title>
<script type="text/javascript" src="https://api.tgos.tw/TGOS_API/tgos?ver=2&AppID=YTvBoU3+KlGNnQY0bVrcBWPZ6C7ghKJQsdnrE+DmFV/brGnGWAG39Q==&APIKey=cGEErDNy5yN/1fQ0vyTOZrghjE+jIU6ufThSqYm3hjzSaK8pYEgKKmkHcoqPE1UtE4fEanYtjmApOs5vhN+nxmCK7qBmwst6H58Hn7rqQDI7x0dHVM+h1Wchuje3Of208tsorv9DnMKDrL6/gu3ISGf/+J3cAuXvDJjIOrPMFxUaaFh2EiT5YxVwZioViKXseoUe6S9XFbA+I2qRj7ifJ8mCWkY0hwAiftTN+yzqflOZDkRsBbKbhW8GCjq9BTniO3WWghoHMOKLqr0P/RRMW10/lVQFh2X+Q8sNR22Ha6AhaLL51CaoVeWYDnyixQFgt5cblMNk9rNxbP5Hr1Cs17AB6kxnF1Is3ubau3d2Co7r99gmJSq0qQ==" charset="utf-8"></script>
<!--下載後請將yourID及yourkey取代為您申請所取得的APPID及APIKEY方能正確顯示服務-->
<script type="text/javascript"> 	
	var pMap = null;
    var WMSLayer = null;	//宣告一個空的變數, 準備承接WMS物件使用
	var WMSLayers = new Array();
	var addrlocate = new TGOS.TGAddress();

	var addrMarker = null; //鄰近地址點
	var clickMarker = null; //點擊位置點
	var markerImg = new TGOS.TGImage("https://api.tgos.tw/TGOS_API/images/marker.png",
					new TGOS.TGSize(33, 33), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(16, 33));
	var markerImg1 = new TGOS.TGImage("https://api.tgos.tw/TGOS_API/images/marker.png",
					new TGOS.TGSize(32, 32), new TGOS.TGPoint(0, 0), new TGOS.TGPoint(16, 32));
					
	 
					
	function InitWnd() {
		navigator.geolocation.getCurrentPosition(callback);  
        function callback(position){  
		document.getElementById("startlon").value = position.coords.longitude;
		document.getElementById("startlat").value = position.coords.latitude;
		var startlon1 = position.coords.longitude; 
		var startlat1 = position.coords.latitude; 
          
		var pOMap = document.getElementById("TGMap");
	    	var mapOptions = {
			mapTypeControl: true,		//mapTypeControl(關閉地圖類型控制項)
			navigationControl: true,	//navigationControl(關閉縮放控制列)
			scaleControl: true			//scaleControl(關閉比例尺控制項)
	    	};
		pMap = new TGOS.TGOnlineMap(pOMap, TGOS.TGCoordSys.EPSG3857, mapOptions);	//宣告TGOnlineMap地圖物件並設定坐標系統
		pMap.setZoom(16);	//指定地圖起始層級
		pMap.setCenter(new TGOS.TGPoint(startlon1, startlat1));	//指定地圖起始中心點坐標	
		
		    var MakerOptions = {
			clickable: false,
			draggable: false,
			flat: true,
			zIndex:40
		    };
			
	    	TGOS.TGEvent.addListener(pMap, "click", function (e) {	//加入滑鼠單擊地圖事件監聽器
			if (addrMarker != null){
				addrMarker.setMap(null);
			}
			if (clickMarker != null){
				clickMarker.setMap(null);
			}
			var pt = e.point;	//取得滑鼠點擊位置坐標
			var px = pt.x;
			var py = pt.y;
			var addrpt = new TGOS.TGPoint(px, py);  //將滑鼠點擊位置轉為TGOS Point點資料
			var message = document.getElementById("title").value + "\n經度: " + pt.x + "\n緯度: " + pt.y ;	//顯示標題+座標
			clickMarker = new TGOS.TGMarker(pMap, addrpt, message, markerImg1, MakerOptions);
			document.getElementById("lon1").value = px;
			document.getElementById("lat1").value = py;
                      document.getElementById("adddistrict1").value = '' ;
                      document.getElementById("adddistrict2").value = '' ;
					  document.getElementById("addlocation").value = '' ;
			addrlocate.nearestAddress(addrpt, TGOS.TGCoordSys.EPSG3857,  //利用滑鼠點擊位置查詢最鄰近地址
				 function(result, status){
					var addrcon = result.formattedAddress;  //取得最鄰近地址查詢結果              
					var addrpoint = result.geometry.location; //取得最鄰近地址xy座標
                    var addrcon1 = result.addressComponents.county 	;  //取得最鄰近縣市查詢結果
                    var addrcon2 = result.addressComponents.town 	;  //取得最鄰近縣市查詢結果
                    if(status == TGOS.TGAddressStatus.OK){
                      document.getElementById("adddistrict1").value = addrcon1 ;
                      document.getElementById("adddistrict2").value = addrcon2 ;
					  document.getElementById("addlocation").value = addrcon ;
                    }
                    else{
                      document.getElementById("adddistrict1").value = '無法取得' ;
                      document.getElementById("adddistrict2").value = '無法取得' ;
					  document.getElementById("addlocation").value = '無法取得' ;
                    }
				   }
			     );
 
		    });
	    }
	}
	
	function addwms(){
			var tUrl = 'http://211.22.161.200/arcgis/services/ALDOC_WMS/TALIS_WMS_SOIL/MapServer/WMSServer?version=1.3.0&request=GetMap&CRS=CRS:84&bbox=119.123390,21.666314,122.902686,25.689753&width=344&height=400&styles=default&format=image/png&layers=9&SRS=EPSG:3857';  //取出WMS連結
			WMSLayer = new TGOS.TGWmsLayer(tUrl, {         //建立WMS物件, 加入WMS連結, 並指定相關屬性	                                          	
				map: pMap,
                preserveViewport: false,
                zIndex:10,
                wsVisible: true,
				opacity: 0.5
			});
          WMSLayers.push(WMSLayer);
		}
	function deletewms(){
			if (WMSLayers.length > 0) {
				for(var i = 0; i < WMSLayers.length; i++)
				{
					WMSLayers[i].removeWmsLayer();	//當圖面上存在WMS圖層時, 將該圖層移除
				}
			} 		
	}
	
	
</script>            
</head>
<body style="margin:0px" onLoad="InitWnd();">
	<div id="TGMap" style="width:100%; height:480px;border: 1px solid #C0C0C0;"></div>
	 <div style="width:100%; height:120px;border: 1px solid #C0C0C0;">
	    起點經度:<input type="text" name="startlon" id="startlon" value="0" size="20"/>
        起點緯度:<input type="text" name="startlat" id="startlat" value="0" size="20"/>
       <br>
       <form action="map-click-tgos-archive.php" method="post">
  	    標記標題:<input type="text" name="title" id="title" value="標記1" size="20"/>
        標記經度:<input type="text" name="lon1" id="lon1" value="0" size="20"/>
        標記緯度:<input type="text" name="lat1" id="lat1" value="0" size="20"/>
        <br>
        標記點行政區:<input type="text" name="adddistrict1" id="adddistrict1" value="無" size="20"/>
         <input type="text" name="adddistrict2" id="adddistrict2" value="無" size="20"/>
        <br>
        鄰近參考地址:<input type="text" name="addlocation" id="addlocation" value="無" size="60"/>
        <br>
        <input type="submit" value="儲存地點">
       </form>  
	 <!--建立下拉選單, 提供預設兩組KML網址-->
	  <select id="urlList">
        <option>土壤特性</option>
   	  </select>
	  <input type="button" value="加入參考圖層" onClick="addwms();"><input type="button" value="移除參考圖層" onClick="deletewms();">
			
		
	</div>
</body>
</html>                                                         